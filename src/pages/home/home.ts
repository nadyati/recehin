import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { OtpPage } from '../otp/otp';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  email:string;
  password:string;
  constructor(public navCtrl: NavController) {

  }
  login(){
    
    this.navCtrl.push(OtpPage);
  }

  goRegister(){
    this.navCtrl.push(RegisterPage);
  }
}
