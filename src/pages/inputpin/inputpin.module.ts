import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InputpinPage } from './inputpin';

@NgModule({
  declarations: [
    InputpinPage,
  ],
  imports: [
    IonicPageModule.forChild(InputpinPage),
  ],
})
export class InputpinPageModule {}
